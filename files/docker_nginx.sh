#!/bin/bash
# Install scripts for docker nginx container
# Reference: https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-16-04

# Adds the GPG key for the official Docker repository to the system
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Add Docker repository to APT sources
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

# Update the packages
apt-get update

# Making sure installing Docker from Repo instead of default Ubuntu 16.04 repo
apt-cache policy docker-ce

# Install Docker Community Edition
apt-get install -y docker-ce

# Pull Nginx image 1.18.0
docker pull nginx:1.18.0

apt-get -y install coreutils

# Create Custom index.html
mkdir -p $home/dockershare/html && echo -e "Wai Kit" >> $home/dockershare/html/index.html

# Run docker with custom index.html and version 1.18.0
docker run -d --name nginx -p 80:80 -v $home/dockershare/html:/usr/share/nginx/html nginx:1.18.0

