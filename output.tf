# Output VPC id
output "vpc_id" {
  value = "${aws_vpc.vpc.id}"
}

# Output public subnet id
output "public_subnet_id" {
  value = "${aws_subnet.public_subnet.*.id}"
}

# Output default security group id
output "default_sg_id" {
  value = "${aws_security_group.default.id}"
}

# Outputs the load balancer DNS name
output "elb_hostname" {
  value = "${aws_elb.nginx.dns_name}"
}

