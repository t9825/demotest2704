# Create security group for Classic load balancer (elb)
resource "aws_security_group" "elb_sg" {
  name        = "${var.environment}-elb-sg"
  description = "The security group will allow HTTP traffic"
  vpc_id      = "${aws_vpc.vpc.id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Makes sure host is pingable
  ingress {
    from_port   = 8
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.environment}-elb-sg"
  }
}

# Create the Classic load balancer (ELB)
resource "aws_elb" "nginx" {
  name            = "${var.environment}-nginx-lb"
  subnets         = "${aws_subnet.public_subnet.*.id}"
  security_groups = ["${aws_security_group.elb_sg.id}"]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }
  listener {
    instance_port     = 22
    instance_protocol = "tcp"
    lb_port           = 22
    lb_protocol       = "tcp"
  }

    health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "TCP:22"
    interval            = 30
  }

  instances                 = "${aws_instance.ec2nginx.*.id}"
  cross_zone_load_balancing = false

  tags = {
    Name        = "${var.environment}-nginx-lb"
  }
}
