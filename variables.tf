# AWS ACCESS KEY
variable "access_key" {
    description = "AWS user access key"
}

# AWS SECRET KEY
variable "secret_key" {
    description = "AWS user secret key"
}

# CIDR Information for VPC, public subnets
variable "vpc_cidr" {
  description = "The CIDR block for the VPC"
}

variable "environment" {
  description = "The environment currently on"
}

variable "public_subnet_cidr" {
  description = "The CIDR block for the public subnet"
}

variable "private_subnet_cidr" {
  description = "The CIDR block for the private subnet"
}

# Current region to use
variable "region" {
  description = "The region to launch instances"
}

# Public Key to access EC2 instances
variable "key_name" {
  description = "The public key for EC2 instances"
}

# Total instances to provision
variable "instance_count" {
  description = "Total number of instances"
}

# The amis to use
variable "amis" {
  type        = map
  description = "Using default ami ubuntu 20.04"

  default = {
    "us-east-1" = "ami-04505e74c0741db8d"
  }
}

# Type of instance to launch
variable "instance_type" {
  description = "The instance type to launch"
}
