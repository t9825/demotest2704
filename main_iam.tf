resource "aws_iam_user" "user" {
  name = "DemoUser"
}

resource "aws_iam_policy_attachment" "demo-attach" {
  name       = "demo-attachment"
  users      = [aws_iam_user.user.name]
  policy_arn = "arn:aws:iam::aws:policy/ReadOnlyAccess"
}